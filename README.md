## Guess My Birthday Game


## Description
Simple python game that will allow user to input birthday in digits and the program will randomly generate their own birthday and will tell you if your guess is correct or not.

## Badges
No shields as of right now, will add later.


## Installation
runs on python3.10.6. 


## Roadmap
Not sure if I will update, but will definitely come back to this project.

## Contributing
Open to contribution

## Support
You can reach me by gitlab

## Authors and acknowledgment
Kevin Ou
, credits to Hack Reactor


## Project status
Work in progress.
